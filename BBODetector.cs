﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class BBODetector : EditorWindow {
    [MenuItem("GameObject/Bad Builder Object Detector")]

    public static void Run()
    {
        var badObjects = new List<Object>();
        var builderObjects = GameObject.FindObjectsOfType(typeof(pb_Object)) as pb_Object[];

        foreach (var builderObject in builderObjects)
        {
            bool isHidden = builderObject.renderer.sharedMaterials.All(x => x.name.Equals("NoDraw"));
            if (isHidden)
                badObjects.Add(builderObject.gameObject);
        }

        Debug.Log("BBOs found: " + badObjects.Count);
        Selection.objects = badObjects.ToArray();
    }
}
